from textblob import TextBlob, Word
import sys
import random
import os

filename=sys.argv[-1]
with open(filename,'r') as f:
      text=f.read().decode('ascii', errors="ignore")
blob = TextBlob(text)

nouns = list()
for word, tag in blob.tags:
	if tag == 'NN':
		nouns.append(word.lemmatize())

print ("This text is about...")
for item in random.sample(nouns, 5):
	word = Word(item)
	print (word.pluralize())

