import nltk
my_sent = "Abdul Kalam (15 October 1931 – 27 July 2015), was the 11th President of India from 2002 to 2007. A career scientist turned statesman, Kalam was born and raised in Rameswaram, Tamil Nadu, and studied physics and aerospace engineering. He spent the next four decades as a scientist and science administrator, mainly at the Defence Research and Development Organisation (DRDO) and Indian Space Research Organisation (ISRO) and was intimately involved in India's civilian space programme and military missile development efforts. He thus came to be known as the Missile Man of India for his work on the development of ballistic missile and launch vehicle technology. He also played a pivotal organisational, technical, and political role in India's Pokhran-II nuclear tests in 1998, the first since the original nuclear test by India in 1974."
text=nltk.sent_tokenize(my_sent)
print (text)
#POS tagging then chunking and Named entity recognition on 1st sentence
#for i in len(text):
parse_tree = nltk.ne_chunk(nltk.tag.pos_tag(text[0].split()), binary=True)
print (parse_tree)

#Saving the list of Names found in Named entity recognition
named_entities = []
for t in parse_tree.subtrees():
    if t.label() == 'NE':
        named_entities.append(list(t))  #to save a list of tagged words instead of a tree
print(named_entities)

#Taking the name from the list 
for x in named_entities:   
       word1 = x
       print (word1)                
       for x in word1:                  
         word2 = x
         print(word2)
         (word3,tag)=word2
       break;
#Replacing the pronoun with the name and displaying the final output.
a=[]
#str="word3"
b=nltk.pos_tag(nltk.word_tokenize(text[4]))
for word, pos in b:
   a.append(word)
   if pos == 'PRP': 
        for i,item in enumerate(a):
            if word == item:              
              a[i]=word3
output=text[0]+' '+' '.join(a)
print(output) 